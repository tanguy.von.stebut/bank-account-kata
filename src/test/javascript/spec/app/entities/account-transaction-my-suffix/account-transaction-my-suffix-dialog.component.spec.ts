/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { BakTestModule } from '../../../test.module';
import { AccountTransactionMySuffixDialogComponent } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix-dialog.component';
import { AccountTransactionMySuffixService } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.service';
import { AccountTransactionMySuffix } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.model';
import { BankAccountMySuffixService } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix';

describe('Component Tests', () => {

    describe('AccountTransactionMySuffix Management Dialog Component', () => {
        let comp: AccountTransactionMySuffixDialogComponent;
        let fixture: ComponentFixture<AccountTransactionMySuffixDialogComponent>;
        let service: AccountTransactionMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [AccountTransactionMySuffixDialogComponent],
                providers: [
                    BankAccountMySuffixService,
                    AccountTransactionMySuffixService
                ]
            })
            .overrideTemplate(AccountTransactionMySuffixDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AccountTransactionMySuffixDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccountTransactionMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new AccountTransactionMySuffix(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.accountTransaction = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'accountTransactionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new AccountTransactionMySuffix();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.accountTransaction = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'accountTransactionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
