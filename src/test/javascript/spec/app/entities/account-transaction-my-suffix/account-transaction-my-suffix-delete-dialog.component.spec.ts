/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { BakTestModule } from '../../../test.module';
import { AccountTransactionMySuffixDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix-delete-dialog.component';
import { AccountTransactionMySuffixService } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.service';

describe('Component Tests', () => {

    describe('AccountTransactionMySuffix Management Delete Component', () => {
        let comp: AccountTransactionMySuffixDeleteDialogComponent;
        let fixture: ComponentFixture<AccountTransactionMySuffixDeleteDialogComponent>;
        let service: AccountTransactionMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [AccountTransactionMySuffixDeleteDialogComponent],
                providers: [
                    AccountTransactionMySuffixService
                ]
            })
            .overrideTemplate(AccountTransactionMySuffixDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AccountTransactionMySuffixDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccountTransactionMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
