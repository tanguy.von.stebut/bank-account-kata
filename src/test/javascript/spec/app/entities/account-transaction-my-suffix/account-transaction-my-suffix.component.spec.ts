/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { BakTestModule } from '../../../test.module';
import { AccountTransactionMySuffixComponent } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.component';
import { AccountTransactionMySuffixService } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.service';
import { AccountTransactionMySuffix } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.model';

describe('Component Tests', () => {

    describe('AccountTransactionMySuffix Management Component', () => {
        let comp: AccountTransactionMySuffixComponent;
        let fixture: ComponentFixture<AccountTransactionMySuffixComponent>;
        let service: AccountTransactionMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [AccountTransactionMySuffixComponent],
                providers: [
                    AccountTransactionMySuffixService
                ]
            })
            .overrideTemplate(AccountTransactionMySuffixComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AccountTransactionMySuffixComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccountTransactionMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new AccountTransactionMySuffix(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.accountTransactions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
