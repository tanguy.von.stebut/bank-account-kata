/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { BakTestModule } from '../../../test.module';
import { AccountTransactionMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix-detail.component';
import { AccountTransactionMySuffixService } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.service';
import { AccountTransactionMySuffix } from '../../../../../../main/webapp/app/entities/account-transaction-my-suffix/account-transaction-my-suffix.model';

describe('Component Tests', () => {

    describe('AccountTransactionMySuffix Management Detail Component', () => {
        let comp: AccountTransactionMySuffixDetailComponent;
        let fixture: ComponentFixture<AccountTransactionMySuffixDetailComponent>;
        let service: AccountTransactionMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [AccountTransactionMySuffixDetailComponent],
                providers: [
                    AccountTransactionMySuffixService
                ]
            })
            .overrideTemplate(AccountTransactionMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AccountTransactionMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AccountTransactionMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new AccountTransactionMySuffix(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.accountTransaction).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
