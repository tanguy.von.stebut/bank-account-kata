/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { BakTestModule } from '../../../test.module';
import { BankAccountMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix-detail.component';
import { BankAccountMySuffixService } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.service';
import { BankAccountMySuffix } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.model';

describe('Component Tests', () => {

    describe('BankAccountMySuffix Management Detail Component', () => {
        let comp: BankAccountMySuffixDetailComponent;
        let fixture: ComponentFixture<BankAccountMySuffixDetailComponent>;
        let service: BankAccountMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [BankAccountMySuffixDetailComponent],
                providers: [
                    BankAccountMySuffixService
                ]
            })
            .overrideTemplate(BankAccountMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new BankAccountMySuffix(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.bankAccount).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
