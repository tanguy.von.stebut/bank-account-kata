/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { BakTestModule } from '../../../test.module';
import { BankAccountMySuffixDialogComponent } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix-dialog.component';
import { BankAccountMySuffixService } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.service';
import { BankAccountMySuffix } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.model';
import { CustomerMySuffixService } from '../../../../../../main/webapp/app/entities/customer-my-suffix';

describe('Component Tests', () => {

    describe('BankAccountMySuffix Management Dialog Component', () => {
        let comp: BankAccountMySuffixDialogComponent;
        let fixture: ComponentFixture<BankAccountMySuffixDialogComponent>;
        let service: BankAccountMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [BankAccountMySuffixDialogComponent],
                providers: [
                    CustomerMySuffixService,
                    BankAccountMySuffixService
                ]
            })
            .overrideTemplate(BankAccountMySuffixDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountMySuffixDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new BankAccountMySuffix(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.bankAccount = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'bankAccountListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new BankAccountMySuffix();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.bankAccount = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'bankAccountListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
