/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { BakTestModule } from '../../../test.module';
import { BankAccountMySuffixComponent } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.component';
import { BankAccountMySuffixService } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.service';
import { BankAccountMySuffix } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.model';

describe('Component Tests', () => {

    describe('BankAccountMySuffix Management Component', () => {
        let comp: BankAccountMySuffixComponent;
        let fixture: ComponentFixture<BankAccountMySuffixComponent>;
        let service: BankAccountMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [BankAccountMySuffixComponent],
                providers: [
                    BankAccountMySuffixService
                ]
            })
            .overrideTemplate(BankAccountMySuffixComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountMySuffixComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new BankAccountMySuffix(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.bankAccounts[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
