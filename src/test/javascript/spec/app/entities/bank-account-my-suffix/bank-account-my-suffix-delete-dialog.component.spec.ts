/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { BakTestModule } from '../../../test.module';
import { BankAccountMySuffixDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix-delete-dialog.component';
import { BankAccountMySuffixService } from '../../../../../../main/webapp/app/entities/bank-account-my-suffix/bank-account-my-suffix.service';

describe('Component Tests', () => {

    describe('BankAccountMySuffix Management Delete Component', () => {
        let comp: BankAccountMySuffixDeleteDialogComponent;
        let fixture: ComponentFixture<BankAccountMySuffixDeleteDialogComponent>;
        let service: BankAccountMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BakTestModule],
                declarations: [BankAccountMySuffixDeleteDialogComponent],
                providers: [
                    BankAccountMySuffixService
                ]
            })
            .overrideTemplate(BankAccountMySuffixDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountMySuffixDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
