import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('AccountTransaction e2e test', () => {

    let navBarPage: NavBarPage;
    let accountTransactionDialogPage: AccountTransactionDialogPage;
    let accountTransactionComponentsPage: AccountTransactionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AccountTransactions', () => {
        navBarPage.goToEntity('account-transaction-my-suffix');
        accountTransactionComponentsPage = new AccountTransactionComponentsPage();
        expect(accountTransactionComponentsPage.getTitle()).toMatch(/bakApp.accountTransaction.home.title/);

    });

    it('should load create AccountTransaction dialog', () => {
        accountTransactionComponentsPage.clickOnCreateButton();
        accountTransactionDialogPage = new AccountTransactionDialogPage();
        expect(accountTransactionDialogPage.getModalTitle()).toMatch(/bakApp.accountTransaction.home.createOrEditLabel/);
        accountTransactionDialogPage.close();
    });

    it('should create and save AccountTransactions', () => {
        accountTransactionComponentsPage.clickOnCreateButton();
        accountTransactionDialogPage.setAmountInput('5');
        expect(accountTransactionDialogPage.getAmountInput()).toMatch('5');
        accountTransactionDialogPage.setTxDateInput('2000-12-31');
        expect(accountTransactionDialogPage.getTxDateInput()).toMatch('2000-12-31');
        accountTransactionDialogPage.typeSelectLastOption();
        accountTransactionDialogPage.bankAccountSelectLastOption();
        accountTransactionDialogPage.save();
        expect(accountTransactionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AccountTransactionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-account-transaction-my-suffix div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AccountTransactionDialogPage {
    modalTitle = element(by.css('h4#myAccountTransactionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    amountInput = element(by.css('input#field_amount'));
    txDateInput = element(by.css('input#field_txDate'));
    typeSelect = element(by.css('select#field_type'));
    bankAccountSelect = element(by.css('select#field_bankAccount'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setAmountInput = function(amount) {
        this.amountInput.sendKeys(amount);
    }

    getAmountInput = function() {
        return this.amountInput.getAttribute('value');
    }

    setTxDateInput = function(txDate) {
        this.txDateInput.sendKeys(txDate);
    }

    getTxDateInput = function() {
        return this.txDateInput.getAttribute('value');
    }

    setTypeSelect = function(type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function() {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function() {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    bankAccountSelectLastOption = function() {
        this.bankAccountSelect.all(by.tagName('option')).last().click();
    }

    bankAccountSelectOption = function(option) {
        this.bankAccountSelect.sendKeys(option);
    }

    getBankAccountSelect = function() {
        return this.bankAccountSelect;
    }

    getBankAccountSelectedOption = function() {
        return this.bankAccountSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
