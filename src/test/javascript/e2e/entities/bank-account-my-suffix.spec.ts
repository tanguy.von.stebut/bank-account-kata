import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('BankAccount e2e test', () => {

    let navBarPage: NavBarPage;
    let bankAccountDialogPage: BankAccountDialogPage;
    let bankAccountComponentsPage: BankAccountComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load BankAccounts', () => {
        navBarPage.goToEntity('bank-account-my-suffix');
        bankAccountComponentsPage = new BankAccountComponentsPage();
        expect(bankAccountComponentsPage.getTitle()).toMatch(/bakApp.bankAccount.home.title/);

    });

    it('should load create BankAccount dialog', () => {
        bankAccountComponentsPage.clickOnCreateButton();
        bankAccountDialogPage = new BankAccountDialogPage();
        expect(bankAccountDialogPage.getModalTitle()).toMatch(/bakApp.bankAccount.home.createOrEditLabel/);
        bankAccountDialogPage.close();
    });

    it('should create and save BankAccounts', () => {
        bankAccountComponentsPage.clickOnCreateButton();
        bankAccountDialogPage.setAccountNumberInput('accountNumber');
        expect(bankAccountDialogPage.getAccountNumberInput()).toMatch('accountNumber');
        bankAccountDialogPage.setBalanceInput('5');
        expect(bankAccountDialogPage.getBalanceInput()).toMatch('5');
        bankAccountDialogPage.customerSelectLastOption();
        bankAccountDialogPage.save();
        expect(bankAccountDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BankAccountComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-bank-account-my-suffix div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BankAccountDialogPage {
    modalTitle = element(by.css('h4#myBankAccountLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    accountNumberInput = element(by.css('input#field_accountNumber'));
    balanceInput = element(by.css('input#field_balance'));
    customerSelect = element(by.css('select#field_customer'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setAccountNumberInput = function(accountNumber) {
        this.accountNumberInput.sendKeys(accountNumber);
    }

    getAccountNumberInput = function() {
        return this.accountNumberInput.getAttribute('value');
    }

    setBalanceInput = function(balance) {
        this.balanceInput.sendKeys(balance);
    }

    getBalanceInput = function() {
        return this.balanceInput.getAttribute('value');
    }

    customerSelectLastOption = function() {
        this.customerSelect.all(by.tagName('option')).last().click();
    }

    customerSelectOption = function(option) {
        this.customerSelect.sendKeys(option);
    }

    getCustomerSelect = function() {
        return this.customerSelect;
    }

    getCustomerSelectedOption = function() {
        return this.customerSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
