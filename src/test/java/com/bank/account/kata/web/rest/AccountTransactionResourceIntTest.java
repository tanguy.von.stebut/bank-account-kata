package com.bank.account.kata.web.rest;

import com.bank.account.kata.BakApp;

import com.bank.account.kata.domain.AccountTransaction;
import com.bank.account.kata.repository.AccountTransactionRepository;
import com.bank.account.kata.service.AccountTransactionService;
import com.bank.account.kata.service.dto.AccountTransactionDTO;
import com.bank.account.kata.service.mapper.AccountTransactionMapper;
import com.bank.account.kata.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.bank.account.kata.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bank.account.kata.domain.enumeration.Transactions;
/**
 * Test class for the AccountTransactionResource REST controller.
 *
 * @see AccountTransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BakApp.class)
public class AccountTransactionResourceIntTest {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final LocalDate DEFAULT_TX_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TX_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Transactions DEFAULT_TYPE = Transactions.WITHDRAWAL;
    private static final Transactions UPDATED_TYPE = Transactions.DEPOSIT;

    @Autowired
    private AccountTransactionRepository accountTransactionRepository;

    @Autowired
    private AccountTransactionMapper accountTransactionMapper;

    @Autowired
    private AccountTransactionService accountTransactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAccountTransactionMockMvc;

    private AccountTransaction accountTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AccountTransactionResource accountTransactionResource = new AccountTransactionResource(accountTransactionService);
        this.restAccountTransactionMockMvc = MockMvcBuilders.standaloneSetup(accountTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountTransaction createEntity(EntityManager em) {
        AccountTransaction accountTransaction = new AccountTransaction()
            .amount(DEFAULT_AMOUNT)
            .txDate(DEFAULT_TX_DATE)
            .type(DEFAULT_TYPE);
        return accountTransaction;
    }

    @Before
    public void initTest() {
        accountTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccountTransaction() throws Exception {
        int databaseSizeBeforeCreate = accountTransactionRepository.findAll().size();

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);
        restAccountTransactionMockMvc.perform(post("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the AccountTransaction in the database
        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        AccountTransaction testAccountTransaction = accountTransactionList.get(accountTransactionList.size() - 1);
        assertThat(testAccountTransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testAccountTransaction.getTxDate()).isEqualTo(DEFAULT_TX_DATE);
        assertThat(testAccountTransaction.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createAccountTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accountTransactionRepository.findAll().size();

        // Create the AccountTransaction with an existing ID
        accountTransaction.setId(1L);
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccountTransactionMockMvc.perform(post("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccountTransaction in the database
        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = accountTransactionRepository.findAll().size();
        // set the field null
        accountTransaction.setAmount(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc.perform(post("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTxDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = accountTransactionRepository.findAll().size();
        // set the field null
        accountTransaction.setTxDate(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc.perform(post("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = accountTransactionRepository.findAll().size();
        // set the field null
        accountTransaction.setType(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc.perform(post("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAccountTransactions() throws Exception {
        // Initialize the database
        accountTransactionRepository.saveAndFlush(accountTransaction);

        // Get all the accountTransactionList
        restAccountTransactionMockMvc.perform(get("/api/account-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].txDate").value(hasItem(DEFAULT_TX_DATE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getAccountTransaction() throws Exception {
        // Initialize the database
        accountTransactionRepository.saveAndFlush(accountTransaction);

        // Get the accountTransaction
        restAccountTransactionMockMvc.perform(get("/api/account-transactions/{id}", accountTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(accountTransaction.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.txDate").value(DEFAULT_TX_DATE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccountTransaction() throws Exception {
        // Get the accountTransaction
        restAccountTransactionMockMvc.perform(get("/api/account-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccountTransaction() throws Exception {
        // Initialize the database
        accountTransactionRepository.saveAndFlush(accountTransaction);
        int databaseSizeBeforeUpdate = accountTransactionRepository.findAll().size();

        // Update the accountTransaction
        AccountTransaction updatedAccountTransaction = accountTransactionRepository.findOne(accountTransaction.getId());
        // Disconnect from session so that the updates on updatedAccountTransaction are not directly saved in db
        em.detach(updatedAccountTransaction);
        updatedAccountTransaction
            .amount(UPDATED_AMOUNT)
            .txDate(UPDATED_TX_DATE)
            .type(UPDATED_TYPE);
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(updatedAccountTransaction);

        restAccountTransactionMockMvc.perform(put("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isOk());

        // Validate the AccountTransaction in the database
        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeUpdate);
        AccountTransaction testAccountTransaction = accountTransactionList.get(accountTransactionList.size() - 1);
        assertThat(testAccountTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testAccountTransaction.getTxDate()).isEqualTo(UPDATED_TX_DATE);
        assertThat(testAccountTransaction.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAccountTransaction() throws Exception {
        int databaseSizeBeforeUpdate = accountTransactionRepository.findAll().size();

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAccountTransactionMockMvc.perform(put("/api/account-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the AccountTransaction in the database
        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAccountTransaction() throws Exception {
        // Initialize the database
        accountTransactionRepository.saveAndFlush(accountTransaction);
        int databaseSizeBeforeDelete = accountTransactionRepository.findAll().size();

        // Get the accountTransaction
        restAccountTransactionMockMvc.perform(delete("/api/account-transactions/{id}", accountTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AccountTransaction> accountTransactionList = accountTransactionRepository.findAll();
        assertThat(accountTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountTransaction.class);
        AccountTransaction accountTransaction1 = new AccountTransaction();
        accountTransaction1.setId(1L);
        AccountTransaction accountTransaction2 = new AccountTransaction();
        accountTransaction2.setId(accountTransaction1.getId());
        assertThat(accountTransaction1).isEqualTo(accountTransaction2);
        accountTransaction2.setId(2L);
        assertThat(accountTransaction1).isNotEqualTo(accountTransaction2);
        accountTransaction1.setId(null);
        assertThat(accountTransaction1).isNotEqualTo(accountTransaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountTransactionDTO.class);
        AccountTransactionDTO accountTransactionDTO1 = new AccountTransactionDTO();
        accountTransactionDTO1.setId(1L);
        AccountTransactionDTO accountTransactionDTO2 = new AccountTransactionDTO();
        assertThat(accountTransactionDTO1).isNotEqualTo(accountTransactionDTO2);
        accountTransactionDTO2.setId(accountTransactionDTO1.getId());
        assertThat(accountTransactionDTO1).isEqualTo(accountTransactionDTO2);
        accountTransactionDTO2.setId(2L);
        assertThat(accountTransactionDTO1).isNotEqualTo(accountTransactionDTO2);
        accountTransactionDTO1.setId(null);
        assertThat(accountTransactionDTO1).isNotEqualTo(accountTransactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(accountTransactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(accountTransactionMapper.fromId(null)).isNull();
    }
}
