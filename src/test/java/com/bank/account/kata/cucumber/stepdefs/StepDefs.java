package com.bank.account.kata.cucumber.stepdefs;

import com.bank.account.kata.BakApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = BakApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
