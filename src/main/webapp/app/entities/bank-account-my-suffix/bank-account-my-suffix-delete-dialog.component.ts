import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BankAccountMySuffix } from './bank-account-my-suffix.model';
import { BankAccountMySuffixPopupService } from './bank-account-my-suffix-popup.service';
import { BankAccountMySuffixService } from './bank-account-my-suffix.service';

@Component({
    selector: 'jhi-bank-account-my-suffix-delete-dialog',
    templateUrl: './bank-account-my-suffix-delete-dialog.component.html'
})
export class BankAccountMySuffixDeleteDialogComponent {

    bankAccount: BankAccountMySuffix;

    constructor(
        private bankAccountService: BankAccountMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.bankAccountService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'bankAccountListModification',
                content: 'Deleted an bankAccount'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-bank-account-my-suffix-delete-popup',
    template: ''
})
export class BankAccountMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bankAccountPopupService: BankAccountMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bankAccountPopupService
                .open(BankAccountMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
