import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { BankAccountMySuffix } from './bank-account-my-suffix.model';
import { BankAccountMySuffixService } from './bank-account-my-suffix.service';

@Component({
    selector: 'jhi-bank-account-my-suffix-detail',
    templateUrl: './bank-account-my-suffix-detail.component.html'
})
export class BankAccountMySuffixDetailComponent implements OnInit, OnDestroy {

    bankAccount: BankAccountMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bankAccountService: BankAccountMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBankAccounts();
    }

    load(id) {
        this.bankAccountService.find(id).subscribe((bankAccount) => {
            this.bankAccount = bankAccount;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBankAccounts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bankAccountListModification',
            (response) => this.load(this.bankAccount.id)
        );
    }
}
