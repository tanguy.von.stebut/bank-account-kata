import { BaseEntity } from './../../shared';

export class BankAccountMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public accountNumber?: string,
        public balance?: number,
        public customerId?: number,
        public transactions?: BaseEntity[],
    ) {
    }
}
