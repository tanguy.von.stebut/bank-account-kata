import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BakSharedModule } from '../../shared';
import {
    BankAccountMySuffixService,
    BankAccountMySuffixPopupService,
    BankAccountMySuffixComponent,
    BankAccountMySuffixDetailComponent,
    BankAccountMySuffixDialogComponent,
    BankAccountMySuffixPopupComponent,
    BankAccountMySuffixDeletePopupComponent,
    BankAccountMySuffixDeleteDialogComponent,
    bankAccountRoute,
    bankAccountPopupRoute,
    BankAccountMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bankAccountRoute,
    ...bankAccountPopupRoute,
];

@NgModule({
    imports: [
        BakSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BankAccountMySuffixComponent,
        BankAccountMySuffixDetailComponent,
        BankAccountMySuffixDialogComponent,
        BankAccountMySuffixDeleteDialogComponent,
        BankAccountMySuffixPopupComponent,
        BankAccountMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        BankAccountMySuffixComponent,
        BankAccountMySuffixDialogComponent,
        BankAccountMySuffixPopupComponent,
        BankAccountMySuffixDeleteDialogComponent,
        BankAccountMySuffixDeletePopupComponent,
    ],
    providers: [
        BankAccountMySuffixService,
        BankAccountMySuffixPopupService,
        BankAccountMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BakBankAccountMySuffixModule {}
