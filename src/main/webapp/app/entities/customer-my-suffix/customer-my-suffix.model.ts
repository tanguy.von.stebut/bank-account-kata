import { BaseEntity } from './../../shared';

export class CustomerMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public accounts?: BaseEntity[],
    ) {
    }
}
