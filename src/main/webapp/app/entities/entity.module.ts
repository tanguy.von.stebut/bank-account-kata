import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BakCustomerMySuffixModule } from './customer-my-suffix/customer-my-suffix.module';
import { BakBankAccountMySuffixModule } from './bank-account-my-suffix/bank-account-my-suffix.module';
import { BakAccountTransactionMySuffixModule } from './account-transaction-my-suffix/account-transaction-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BakCustomerMySuffixModule,
        BakBankAccountMySuffixModule,
        BakAccountTransactionMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BakEntityModule {}
