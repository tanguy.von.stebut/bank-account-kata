import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BakSharedModule } from '../../shared';
import {
    AccountTransactionMySuffixService,
    AccountTransactionMySuffixPopupService,
    AccountTransactionMySuffixComponent,
    AccountTransactionMySuffixDetailComponent,
    AccountTransactionMySuffixDialogComponent,
    AccountTransactionMySuffixLoadComponent,
    AccountTransactionMySuffixLoadPopupComponent,
    AccountTransactionMySuffixPopupComponent,
    AccountTransactionMySuffixDeletePopupComponent,
    AccountTransactionMySuffixDeleteDialogComponent,
    accountTransactionRoute,
    accountTransactionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...accountTransactionRoute,
    ...accountTransactionPopupRoute,
];

@NgModule({
    imports: [
        BakSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AccountTransactionMySuffixComponent,
        AccountTransactionMySuffixDetailComponent,
        AccountTransactionMySuffixDialogComponent,
        AccountTransactionMySuffixLoadComponent,
        AccountTransactionMySuffixDeleteDialogComponent,
        AccountTransactionMySuffixPopupComponent,
        AccountTransactionMySuffixLoadPopupComponent,
        AccountTransactionMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        AccountTransactionMySuffixComponent,
        AccountTransactionMySuffixDialogComponent,
        AccountTransactionMySuffixLoadComponent,
        AccountTransactionMySuffixPopupComponent,
        AccountTransactionMySuffixLoadPopupComponent,
        AccountTransactionMySuffixDeleteDialogComponent,
        AccountTransactionMySuffixDeletePopupComponent,
    ],
    providers: [
        AccountTransactionMySuffixService,
        AccountTransactionMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BakAccountTransactionMySuffixModule {}
