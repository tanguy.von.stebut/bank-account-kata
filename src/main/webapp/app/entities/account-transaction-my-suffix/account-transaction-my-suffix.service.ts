import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AccountTransactionMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/account-transactions';
    private resourceFilteredUrl = SERVER_API_URL + 'api/account-transactions/bank-account';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(accountTransaction: AccountTransactionMySuffix): Observable<AccountTransactionMySuffix> {
        const copy = this.convert(accountTransaction);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(accountTransaction: AccountTransactionMySuffix): Observable<AccountTransactionMySuffix> {
        const copy = this.convert(accountTransaction);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AccountTransactionMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    filteredQuery(accountId: number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceFilteredUrl}/${accountId}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AccountTransactionMySuffix.
     */
    private convertItemFromServer(json: any): AccountTransactionMySuffix {
        const entity: AccountTransactionMySuffix = Object.assign(new AccountTransactionMySuffix(), json);
        entity.txDate = this.dateUtils
            .convertLocalDateFromServer(json.txDate);
        return entity;
    }

    /**
     * Convert a AccountTransactionMySuffix to a JSON which can be sent to the server.
     */
    private convert(accountTransaction: AccountTransactionMySuffix): AccountTransactionMySuffix {
        const copy: AccountTransactionMySuffix = Object.assign({}, accountTransaction);
        copy.txDate = this.dateUtils
            .convertLocalDateToServer(accountTransaction.txDate);
        return copy;
    }
}
