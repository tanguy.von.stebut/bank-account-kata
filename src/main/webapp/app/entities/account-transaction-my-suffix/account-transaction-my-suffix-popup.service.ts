import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
import { AccountTransactionMySuffixService } from './account-transaction-my-suffix.service';

@Injectable()
export class AccountTransactionMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private accountTransactionService: AccountTransactionMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, accountId: number, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            console.log('Opening popup with args id=', id, ' isOpen=', isOpen);
            if (isOpen) {
                resolve(this.ngbModalRef);
                // return;
            }

            if (id) {
                this.accountTransactionService.find(id).subscribe((accountTransaction) => {
                    if (accountTransaction.txDate) {
                        accountTransaction.txDate = {
                            year: accountTransaction.txDate.getFullYear(),
                            month: accountTransaction.txDate.getMonth() + 1,
                            day: accountTransaction.txDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.accountTransactionModalRef(component, accountTransaction, accountId);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.accountTransactionModalRef(component, new AccountTransactionMySuffix(), accountId);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    accountTransactionModalRef(component: Component, accountTransaction: AccountTransactionMySuffix, accountId: number): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.accountTransaction = accountTransaction;
        modalRef.componentInstance.bankAccountId = accountId;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
