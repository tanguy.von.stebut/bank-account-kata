import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
import { AccountTransactionMySuffixPopupService } from './account-transaction-my-suffix-popup.service';
import { AccountTransactionMySuffixService } from './account-transaction-my-suffix.service';

@Component({
    selector: 'jhi-account-transaction-my-suffix-delete-dialog',
    templateUrl: './account-transaction-my-suffix-delete-dialog.component.html'
})
export class AccountTransactionMySuffixDeleteDialogComponent {

    accountTransaction: AccountTransactionMySuffix;

    constructor(
        private accountTransactionService: AccountTransactionMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.accountTransactionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'accountTransactionListModification',
                content: 'Deleted an accountTransaction'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-account-transaction-my-suffix-delete-popup',
    template: ''
})
export class AccountTransactionMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private accountTransactionPopupService: AccountTransactionMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.accountTransactionPopupService
                .open(AccountTransactionMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
