import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { BankAccountMySuffix } from '../bank-account-my-suffix/bank-account-my-suffix.model';
import { BankAccountMySuffixService } from '../bank-account-my-suffix/bank-account-my-suffix.service';
import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
import { AccountTransactionMySuffixService } from './account-transaction-my-suffix.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-account-transaction-my-suffix',
    templateUrl: './account-transaction-my-suffix.component.html'
})
export class AccountTransactionMySuffixComponent implements OnInit, OnDestroy {

    accountTransactions: AccountTransactionMySuffix[];
    bankAccount: BankAccountMySuffix = new BankAccountMySuffix();
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    accountId: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;

    constructor(
        private bankAccountService: BankAccountMySuffixService,
        private accountTransactionService: AccountTransactionMySuffixService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private router: Router
    ) {
        this.accountTransactions = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
    }

    loadAll() {
        this.accountTransactionService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAllByAccount(accountId) {
        this.bankAccountService.find(accountId).subscribe((bankAccount) => {
            this.bankAccount = bankAccount;
        });

        this.accountTransactionService.filteredQuery(accountId, {
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    reset(accountId) {
        if ('OK' !== accountId) {
            console.log('Filtering Bankaccount #', accountId);
        }
        this.page = 0;
        this.accountTransactions = [];
        this.bankAccount.id = accountId;
        this.loadAllByAccount(accountId);
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        this.registerChangeInAccountTransactions();
        this.router.navigate(['/', { outlets: { popup: ['account-transaction-my-suffix-init'] } }]);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AccountTransactionMySuffix) {
        return item.id;
    }
    registerChangeInAccountTransactions() {
        this.eventSubscriber = this.eventManager.subscribe('accountTransactionListModification', (response) => this.reset(response.content));
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.accountTransactions.push(data[i]);
        }
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
