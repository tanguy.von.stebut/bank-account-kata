import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AccountTransactionMySuffixComponent } from './account-transaction-my-suffix.component';
import { AccountTransactionMySuffixDetailComponent } from './account-transaction-my-suffix-detail.component';
import { AccountTransactionMySuffixPopupComponent } from './account-transaction-my-suffix-dialog.component';
import { AccountTransactionMySuffixLoadPopupComponent } from './account-transaction-my-suffix-load.component';
import { AccountTransactionMySuffixDeletePopupComponent } from './account-transaction-my-suffix-delete-dialog.component';

export const accountTransactionRoute: Routes = [
    {
        path: 'account-transaction-my-suffix',
        component: AccountTransactionMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bakApp.accountTransaction.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'account-transaction-my-suffix/:id',
        component: AccountTransactionMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bakApp.accountTransaction.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const accountTransactionPopupRoute: Routes = [
    {
        path: 'account-transaction-my-suffix-init',
        component: AccountTransactionMySuffixLoadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bakApp.accountTransaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'account-transaction-my-suffix-new/:accountId',
        component: AccountTransactionMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bakApp.accountTransaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'account-transaction-my-suffix/:id/edit',
        component: AccountTransactionMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bakApp.accountTransaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'account-transaction-my-suffix/:id/delete',
        component: AccountTransactionMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bakApp.accountTransaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
