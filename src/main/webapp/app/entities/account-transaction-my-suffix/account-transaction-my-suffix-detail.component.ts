import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
import { AccountTransactionMySuffixService } from './account-transaction-my-suffix.service';

@Component({
    selector: 'jhi-account-transaction-my-suffix-detail',
    templateUrl: './account-transaction-my-suffix-detail.component.html'
})
export class AccountTransactionMySuffixDetailComponent implements OnInit, OnDestroy {

    accountTransaction: AccountTransactionMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private accountTransactionService: AccountTransactionMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAccountTransactions();
    }

    load(id) {
        this.accountTransactionService.find(id).subscribe((accountTransaction) => {
            this.accountTransaction = accountTransaction;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAccountTransactions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'accountTransactionListModification',
            (response) => this.load(this.accountTransaction.id)
        );
    }
}
