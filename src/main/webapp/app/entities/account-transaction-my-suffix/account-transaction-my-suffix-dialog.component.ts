import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
import { AccountTransactionMySuffixPopupService } from './account-transaction-my-suffix-popup.service';
import { AccountTransactionMySuffixService } from './account-transaction-my-suffix.service';
import { BankAccountMySuffix, BankAccountMySuffixService } from '../bank-account-my-suffix';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-account-transaction-my-suffix-dialog',
    templateUrl: './account-transaction-my-suffix-dialog.component.html'
})
export class AccountTransactionMySuffixDialogComponent implements OnInit {

    accountTransaction: AccountTransactionMySuffix;
    isSaving: boolean;

    eventSubscriber: Subscription;
    bankaccounts: BankAccountMySuffix[];
    bankAccount: BankAccountMySuffix;
    bankAccountId: number;
    txDateDp: any;

    constructor(
        private route: ActivatedRoute,
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private accountTransactionService: AccountTransactionMySuffixService,
        private bankAccountService: BankAccountMySuffixService,
        private eventManager: JhiEventManager
    ) {
        this.bankAccount = new BankAccountMySuffix();
    }

    ngOnInit() {
        this.isSaving = false;
        this.bankAccountService.query()
           .subscribe((res: ResponseWrapper) => { this.bankaccounts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bankAccountService.find(this.bankAccountId).subscribe((bankAccount) => {
            this.bankAccount = bankAccount;
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.accountTransaction.bankAccountId = this.bankAccount.id;
        if (this.accountTransaction.id !== undefined) {
            this.subscribeToSaveResponse(
                this.accountTransactionService.update(this.accountTransaction));
        } else {
            this.subscribeToSaveResponse(
                this.accountTransactionService.create(this.accountTransaction));
        }
    }

    private subscribeToSaveResponse(result: Observable<AccountTransactionMySuffix>) {
        result.subscribe((res: AccountTransactionMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AccountTransactionMySuffix) {
        this.eventManager.broadcast({ name: 'accountTransactionListModification', content: this.accountTransaction.bankAccountId});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBankAccountById(index: number, item: BankAccountMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-account-transaction-my-suffix-popup',
    template: ''
})
export class AccountTransactionMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private accountTransactionPopupService: AccountTransactionMySuffixPopupService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.accountTransactionPopupService
                    .open(AccountTransactionMySuffixDialogComponent as Component, 0, params['id']);
            } else {
                this.accountTransactionPopupService
                    .open(AccountTransactionMySuffixDialogComponent as Component, params['accountId']);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
