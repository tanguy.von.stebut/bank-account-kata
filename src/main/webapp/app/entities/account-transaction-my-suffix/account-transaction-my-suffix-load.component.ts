import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AccountTransactionMySuffix } from './account-transaction-my-suffix.model';
// import { BankAccountMySuffix } from '../bank-account-my-suffix/bank-account-my-suffix.model';
import { AccountTransactionMySuffixPopupService } from './account-transaction-my-suffix-popup.service';
import { BankAccountMySuffix, BankAccountMySuffixService } from '../bank-account-my-suffix';
// import { BankAccountMySuffixService } from '../bank-account-my-suffix';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-account-transaction-my-suffix-load',
    templateUrl: './account-transaction-my-suffix-load.component.html'
})
export class AccountTransactionMySuffixLoadComponent implements OnInit {

    bankAccount: BankAccountMySuffix = new BankAccountMySuffix();
    bankaccounts: BankAccountMySuffix[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private bankAccountService: BankAccountMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        console.log('*************************************** init ***********************************************');
        this.bankAccountService.query()
            .subscribe((res: ResponseWrapper) => { this.bankaccounts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    loadAccount() {
        console.log('Account to load ', this.bankAccount.id);
        this.eventManager.broadcast({ name: 'accountTransactionListModification', content: this.bankAccount.id});
        this.activeModal.dismiss();
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

}

@Component({
    selector: 'jhi-account-transaction-my-suffix-load-popup',
    template: ''
})
export class AccountTransactionMySuffixLoadPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private accountTransactionPopupService: AccountTransactionMySuffixPopupService
    ) {}

    ngOnInit() {
        console.log('*************************************** popup init ***********************************************');
        this.routeSub = this.route.params.subscribe((params) => {
            this.accountTransactionPopupService
                .open(AccountTransactionMySuffixLoadComponent as Component, 0);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
