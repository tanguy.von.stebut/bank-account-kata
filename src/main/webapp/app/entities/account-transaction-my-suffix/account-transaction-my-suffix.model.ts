import { BaseEntity } from './../../shared';

export const enum Transactions {
    'WITHDRAWAL',
    'DEPOSIT'
}

export class AccountTransactionMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public txDate?: any,
        public type?: Transactions,
        public bankAccountId?: number,
    ) {
    }
}
