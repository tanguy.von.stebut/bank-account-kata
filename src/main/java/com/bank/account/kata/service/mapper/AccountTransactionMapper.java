package com.bank.account.kata.service.mapper;

import com.bank.account.kata.domain.*;
import com.bank.account.kata.service.dto.AccountTransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AccountTransaction and its DTO AccountTransactionDTO.
 */
@Mapper(componentModel = "spring", uses = {BankAccountMapper.class})
public interface AccountTransactionMapper extends EntityMapper<AccountTransactionDTO, AccountTransaction> {

    @Mapping(source = "bankAccount.id", target = "bankAccountId")
    AccountTransactionDTO toDto(AccountTransaction accountTransaction); 

    @Mapping(source = "bankAccountId", target = "bankAccount")
    AccountTransaction toEntity(AccountTransactionDTO accountTransactionDTO);

    default AccountTransaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        AccountTransaction accountTransaction = new AccountTransaction();
        accountTransaction.setId(id);
        return accountTransaction;
    }
}
