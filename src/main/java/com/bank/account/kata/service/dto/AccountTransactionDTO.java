package com.bank.account.kata.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.bank.account.kata.domain.enumeration.Transactions;

/**
 * A DTO for the AccountTransaction entity.
 */
public class AccountTransactionDTO implements Serializable {

    private Long id;

    @NotNull
    private Double amount;

    @NotNull
    private LocalDate txDate;

    @NotNull
    private Transactions type;

    private Long bankAccountId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public LocalDate getTxDate() {
        return txDate;
    }

    public void setTxDate(LocalDate txDate) {
        this.txDate = txDate;
    }

    public Transactions getType() {
        return type;
    }

    public void setType(Transactions type) {
        this.type = type;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountTransactionDTO accountTransactionDTO = (AccountTransactionDTO) o;
        if(accountTransactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountTransactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountTransactionDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", txDate='" + getTxDate() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
