package com.bank.account.kata.service;

import com.bank.account.kata.service.dto.AccountTransactionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing AccountTransaction.
 */
public interface AccountTransactionService {

    /**
     * Save a accountTransaction.
     *
     * @param accountTransactionDTO the entity to save
     * @return the persisted entity
     */
    AccountTransactionDTO save(AccountTransactionDTO accountTransactionDTO);

    /**
     * Get all the accountTransactions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AccountTransactionDTO> findAll(Pageable pageable);

    /**
     * Get all the accountTransactions for a given Bank Account.
     *
     * @param accountId the given account id
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AccountTransactionDTO> findAllByAccountId(Long accountId, Pageable pageable);

    /**
     * Get the "id" accountTransaction.
     *
     * @param id the id of the entity
     * @return the entity
     */
    AccountTransactionDTO findOne(Long id);

    /**
     * Delete the "id" accountTransaction.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
