package com.bank.account.kata.service.mapper;

import com.bank.account.kata.domain.*;
import com.bank.account.kata.service.dto.BankAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BankAccount and its DTO BankAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class})
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {

    @Mapping(source = "customer.id", target = "customerId")
    BankAccountDTO toDto(BankAccount bankAccount); 

    @Mapping(source = "customerId", target = "customer")
    @Mapping(target = "transactions", ignore = true)
    BankAccount toEntity(BankAccountDTO bankAccountDTO);

    default BankAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);
        return bankAccount;
    }
}
