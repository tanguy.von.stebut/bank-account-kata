package com.bank.account.kata.service.impl;

import com.bank.account.kata.service.AccountTransactionService;
import com.bank.account.kata.domain.AccountTransaction;
import com.bank.account.kata.domain.BankAccount;
import com.bank.account.kata.domain.enumeration.Transactions;
import com.bank.account.kata.repository.AccountTransactionRepository;
import com.bank.account.kata.repository.BankAccountRepository;
import com.bank.account.kata.service.dto.AccountTransactionDTO;
import com.bank.account.kata.service.mapper.AccountTransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing AccountTransaction.
 */
@Service
@Transactional
public class AccountTransactionServiceImpl implements AccountTransactionService{

    private final Logger log = LoggerFactory.getLogger(AccountTransactionServiceImpl.class);

    private final AccountTransactionRepository accountTransactionRepository;
    
    private final BankAccountRepository bankAccountRepository;

    private final AccountTransactionMapper accountTransactionMapper;

    public AccountTransactionServiceImpl(AccountTransactionRepository accountTransactionRepository, BankAccountRepository bankAccountRepository, AccountTransactionMapper accountTransactionMapper) {
        this.accountTransactionRepository = accountTransactionRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.accountTransactionMapper = accountTransactionMapper;
    }

    /**
     * Save a accountTransaction.
     *
     * @param accountTransactionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AccountTransactionDTO save(AccountTransactionDTO accountTransactionDTO) {
        log.debug("Request to save AccountTransaction : {}", accountTransactionDTO);
        AccountTransaction accountTransaction = accountTransactionMapper.toEntity(accountTransactionDTO);

	final BankAccount bankAccount = bankAccountRepository.findOne(accountTransaction.getBankAccount().getId());

        final double balance = bankAccount.getBalance();
	accountTransaction.setBankAccount(bankAccount);
        log.info("Making a transaction of type : {} , amount {} , on bankAccount {}, which balance is now {}", accountTransaction.getType(), accountTransaction.getAmount(), bankAccount, balance );

        if (accountTransaction.getType().equals(Transactions.DEPOSIT)){
            bankAccount.setBalance(balance + accountTransaction.getAmount());
        }
        else{
            bankAccount.setBalance(balance - accountTransaction.getAmount());
        }
        accountTransaction = accountTransactionRepository.save(accountTransaction);
        return accountTransactionMapper.toDto(accountTransaction);
    }

    /**
     * Get all the accountTransactions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AccountTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AccountTransactions");
        return accountTransactionRepository.findAll(pageable)
            .map(accountTransactionMapper::toDto);
    }

    /**
     * Get all the accountTransactions by Bank-Account.
     *
     * @param accountId the give Account Id 
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AccountTransactionDTO> findAllByAccountId(Long accountId, Pageable pageable) {
        log.debug("Request to get all AccountTransactions by account-id");
        return accountTransactionRepository.findAllByBankAccountId(accountId,pageable)
            .map(accountTransactionMapper::toDto);
    }

    /**
     * Get one accountTransaction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AccountTransactionDTO findOne(Long id) {
        log.debug("Request to get AccountTransaction : {}", id);
        AccountTransaction accountTransaction = accountTransactionRepository.findOne(id);
        return accountTransactionMapper.toDto(accountTransaction);
    }

    /**
     * Delete the accountTransaction by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AccountTransaction : {}", id);
        accountTransactionRepository.delete(id);
    }
}
