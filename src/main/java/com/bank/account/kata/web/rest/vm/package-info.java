/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bank.account.kata.web.rest.vm;
