package com.bank.account.kata.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bank.account.kata.service.AccountTransactionService;
import com.bank.account.kata.web.rest.errors.BadRequestAlertException;
import com.bank.account.kata.web.rest.util.HeaderUtil;
import com.bank.account.kata.web.rest.util.PaginationUtil;
import com.bank.account.kata.service.dto.AccountTransactionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AccountTransaction.
 */
@RestController
@RequestMapping("/api")
public class AccountTransactionResource {

    private final Logger log = LoggerFactory.getLogger(AccountTransactionResource.class);

    private static final String ENTITY_NAME = "accountTransaction";

    private final AccountTransactionService accountTransactionService;

    public AccountTransactionResource(AccountTransactionService accountTransactionService) {
        this.accountTransactionService = accountTransactionService;
    }

    /**
     * POST  /account-transactions : Create a new accountTransaction.
     *
     * @param accountTransactionDTO the accountTransactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accountTransactionDTO, or with status 400 (Bad Request) if the accountTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/account-transactions")
    @Timed
    public ResponseEntity<AccountTransactionDTO> createAccountTransaction(@Valid @RequestBody AccountTransactionDTO accountTransactionDTO) throws URISyntaxException {
        log.debug("REST request to save AccountTransaction : {}", accountTransactionDTO);
        if (accountTransactionDTO.getId() != null) {
            throw new BadRequestAlertException("A new accountTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccountTransactionDTO result = accountTransactionService.save(accountTransactionDTO);
        return ResponseEntity.created(new URI("/api/account-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /account-transactions : Updates an existing accountTransaction.
     *
     * @param accountTransactionDTO the accountTransactionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accountTransactionDTO,
     * or with status 400 (Bad Request) if the accountTransactionDTO is not valid,
     * or with status 500 (Internal Server Error) if the accountTransactionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/account-transactions")
    @Timed
    public ResponseEntity<AccountTransactionDTO> updateAccountTransaction(@Valid @RequestBody AccountTransactionDTO accountTransactionDTO) throws URISyntaxException {
        log.debug("REST request to update AccountTransaction : {}", accountTransactionDTO);
        if (accountTransactionDTO.getId() == null) {
            return createAccountTransaction(accountTransactionDTO);
        }
        AccountTransactionDTO result = accountTransactionService.save(accountTransactionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accountTransactionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /account-transactions : get all the accountTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of accountTransactions in body
     */
    @GetMapping("/account-transactions")
    @Timed
    public ResponseEntity<List<AccountTransactionDTO>> getAllAccountTransactions(Pageable pageable) {
        log.debug("REST request to get a page of AccountTransactions");
        Page<AccountTransactionDTO> page = accountTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/account-transactions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /account-transactions : get all the accountTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of accountTransactions in body
     */
    @GetMapping("/account-transactions/bank-account/{accountId}")
    @Timed
    public ResponseEntity<List<AccountTransactionDTO>> getAllAccountTransactionsByAccount(@PathVariable Long accountId, Pageable pageable) {
        log.debug("REST request to get a page of AccountTransactions for given Account");
        Page<AccountTransactionDTO> page = accountTransactionService.findAllByAccountId(accountId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/account-transactions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /account-transactions/:id : get the "id" accountTransaction.
     *
     * @param id the id of the accountTransactionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountTransactionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/account-transactions/{id}")
    @Timed
    public ResponseEntity<AccountTransactionDTO> getAccountTransaction(@PathVariable Long id) {
        log.debug("REST request to get AccountTransaction : {}", id);
        AccountTransactionDTO accountTransactionDTO = accountTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(accountTransactionDTO));
    }

    /**
     * DELETE  /account-transactions/:id : delete the "id" accountTransaction.
     *
     * @param id the id of the accountTransactionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/account-transactions/{id}")
    @Timed
    public ResponseEntity<Void> deleteAccountTransaction(@PathVariable Long id) {
        log.debug("REST request to delete AccountTransaction : {}", id);
        accountTransactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
