package com.bank.account.kata.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bank.account.kata.domain.AccountTransaction;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AccountTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, Long> {

     Page<AccountTransaction> findAllByBankAccountId(Long accountId, Pageable pageable);
}
