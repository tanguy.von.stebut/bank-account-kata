package com.bank.account.kata.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.bank.account.kata.domain.enumeration.Transactions;

/**
 * A AccountTransaction.
 */
@Entity
@Table(name = "account_transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AccountTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @NotNull
    @Column(name = "tx_date", nullable = false)
    private LocalDate txDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private Transactions type;

    @ManyToOne(fetch = FetchType.EAGER)
    private BankAccount bankAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public AccountTransaction amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public LocalDate getTxDate() {
        return txDate;
    }

    public AccountTransaction txDate(LocalDate txDate) {
        this.txDate = txDate;
        return this;
    }

    public void setTxDate(LocalDate txDate) {
        this.txDate = txDate;
    }

    public Transactions getType() {
        return type;
    }

    public AccountTransaction type(Transactions type) {
        this.type = type;
        return this;
    }

    public void setType(Transactions type) {
        this.type = type;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public AccountTransaction bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountTransaction accountTransaction = (AccountTransaction) o;
        if (accountTransaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountTransaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountTransaction{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", txDate='" + getTxDate() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
