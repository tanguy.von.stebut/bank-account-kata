package com.bank.account.kata.domain.enumeration;

/**
 * The Transactions enumeration.
 */
public enum Transactions {
    WITHDRAWAL, DEPOSIT
}
